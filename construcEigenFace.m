function new_face = construcEigenFace(face_mean, U, S, u)    
    new_face = face_mean + U*S*u(:);
end