function new_p = updateWarpAffine(T0, s0, s, srcimage, p, SD, Hinv)

RS = length(p);
s_target = s*p + s0;

warpedImage = warpMarkings(srcimage, reshape(s_target*128+1, 50, 2), reshape(s0*128+1, 50, 2));

Idiff = double(warpedImage) - double(reshape(T0, 130, 130, 3));

[m,n,~] = size(warpedImage);

SD_transpose_Idiff = zeros(RS,1);
Idiff_at_pixel = zeros(3,1);
for x = 1:n
    for y = 1:m
        SD_at_pixel(:,:) = SD(y,x,:,:);
        Idiff_at_pixel(:,1) = Idiff(y,x,:);
        SD_transpose_Idiff(:,:) = SD_transpose_Idiff(:,:) + SD_at_pixel'*Idiff_at_pixel;
    end
end

dp = Hinv*SD_transpose_Idiff/256;
 
ds_0 = -s*dp;

ds = getIncrementalWarp(s0, ds_0, s_target);
new_p = s' * (s_target + ds - s0);

    
end