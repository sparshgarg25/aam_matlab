function ds = getIncrementalWarp(s0, ds0, s)
%get incremental warp ds at current warp s, when s0+ds0 is warped to s
%in other words: this function solves ds for s0+ds0 -> s+ds  
[fvidx ~] = getFaceVertexIndices();
ds = zeros(100,1);

for i = 1:50
    [Tidx ~] = find(fvidx == i);
    dsi_accum = zeros(1,2);
    for j = 1:length(Tidx)
        triangle_indices =  fvidx(Tidx(j),:);
        other_vectices = triangle_indices(triangle_indices ~= i);
        p0 = zeros(3,2);
        p0(1,:) = s0([i,i+50]);
        p0(2,:) = s0([other_vectices(1),other_vectices(1)+50]);
        p0(3,:) = s0([other_vectices(2),other_vectices(2)+50]);
        
        p1 = zeros(3,2);
        p1(1,:) = s([i,i+50]);
        p1(2,:) = s([other_vectices(1),other_vectices(1)+50]);
        p1(3,:) = s([other_vectices(2),other_vectices(2)+50]);
        
        alpha = (ds0(i)*(p0(3,2)-p0(1,2)) - ds0(i+50)*(p0(3,1)-p0(1,1))) / ...
            ((p0(2,1)-p0(1,1))*(p0(3,2)-p0(1,2)) - (p0(2,2)-p0(1,2))*(p0(3,1)-p0(1,1)));
        
        beta = (-ds0(i)*(p0(2,2)-p0(1,2)) + ds0(i+50)*(p0(2,1)-p0(1,1))) / ...
            ((p0(2,1)-p0(1,1))*(p0(3,2)-p0(1,2)) - (p0(2,2)-p0(1,2))*(p0(3,1)-p0(1,1)));
        
        dsi_accum = dsi_accum + alpha * (p1(2,:)-p1(1,:)) + beta * (p1(3,:) - p1(1,:));
    end
    ds(i) = dsi_accum(1)/length(Tidx);
    ds(i+50) = dsi_accum(2)/length(Tidx);
end
