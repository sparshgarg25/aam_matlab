function [face_image, eye_info, mouth_info] = extractFaceImage(original_image, faceinfo, scale_to_size)

face_y_expansion_factor = 1.1;

rel = 0.4;  % eye level ratio 
rml = 0.80;  % mouth level ratio

[m, n, ~] = size(original_image);

%face_center = [faceinfo(1,1) + faceinfo(1,3)/2, faceinfo(1,2) + faceinfo(1,4)/2];
eye1_center = [faceinfo(2,1) + faceinfo(2,3)/2, faceinfo(2,2) + faceinfo(2,4)/2];
eye2_center = [faceinfo(3,1) + faceinfo(3,3)/2, faceinfo(3,2) + faceinfo(3,4)/2];

mouth_center = [faceinfo(4,1) + faceinfo(4,3)/2, faceinfo(4,2) + faceinfo(4,4)/2];

theta = atan((eye2_center(2)-eye1_center(2))/(eye2_center(1)-eye1_center(1)));

image_rotated = imrotate(original_image, theta * 180 / pi, 'bicubic', 'crop');

R = [cos(theta) sin(theta); -sin(theta) cos(theta)];

%nfc = (face_center - [n/2, m/2])*R' + [n/2, m/2];

nec1 = (eye1_center + faceinfo(1,1:2) - [n/2, m/2])*R' + [n/2, m/2]; % new eye 1 center 
 
nec2 = (eye2_center + faceinfo(1,1:2) - [n/2, m/2])*R' + [n/2, m/2]; % new eye 2 center

nmc = (mouth_center + faceinfo(1,1:2) - [n/2, m/2])*R' + [n/2, m/2]; % new mouth center

nec = (nec1 + nec2)/2; % cental point between 2 eyes

nfc =  (rml-0.5)/(rml-rel) * nec + (0.5 - rel)/(rml-rel) * nmc; % new face center

nfh = 1/(rml-rel) * abs((nmc(2) - nec(2))); % new face height
nfw = nfh / face_y_expansion_factor; % new face width

if nargout == 0
    figure, imshow(original_image);
    rectangle('Position',faceinfo(1,1:4) ,'EdgeColor',[0, 0, 1]);
    rectangle('Position',faceinfo(2,1:4) + [faceinfo(1, 1:2) 0 0],'EdgeColor',[1, 0, 0]);
    rectangle('Position',faceinfo(3,1:4) + [faceinfo(1, 1:2) 0 0],'EdgeColor',[1, 0, 0]);
    rectangle('Position',faceinfo(4,1:4) + [faceinfo(1, 1:2) 0 0],'EdgeColor',[0, 0.5, 1]);
    
    figure, imshow(image_rotated);
    rectangle('Position',[nfc(1) - nfw/2, nfc(2) - nfh/2 nfw, nfh] ,'EdgeColor',[0, 0, 1]);
    
    if faceinfo(2,3)/faceinfo(3,3) > 1.2 || faceinfo(2,3)/faceinfo(3,3)<0.833
        eye_color = [1 0 0];
    else
        eye_color = [0 1 0];
    end
        
    rectangle('Position',[nec1(1) - faceinfo(2,3)/2, nec1(2) - faceinfo(2,4)/2 faceinfo(2,3:4)] ,'EdgeColor',eye_color);
       
    rectangle('Position',[nec2(1) - faceinfo(3,3)/2, nec2(2) - faceinfo(3,4)/2 faceinfo(3,3:4)] ,'EdgeColor',eye_color);
    
    rectangle('Position',[nmc(1) - faceinfo(4,3)/2, nmc(2) - faceinfo(4,4)/2 faceinfo(4,3:4)] ,'EdgeColor',[0, 0.5, 1]);

    
end

%faceinfo(1,4)=faceinfo(1,4)*face_y_expansion_factor;

face_image = image_rotated(max(ceil(nfc(2) - nfh/2),1):min(ceil(nfc(2) + nfh/2),m),...
                            max(ceil(nfc(1) - nfw/2),1):min(ceil(nfc(1) + nfw/2),n), :);
eye_info = [nec1(1) - faceinfo(2,3)/2 - nfc(1) + nfw/2, nec1(2) - faceinfo(2,4)/2 - nfc(2) + nfh/2, faceinfo(2,3:4); ...
            nec2(1) - faceinfo(3,3)/2 - nfc(1) + nfw/2, nec2(2) - faceinfo(3,4)/2 - nfc(2) + nfh/2, faceinfo(3,3:4)];

mouth_info = [nmc(1) - faceinfo(4,3)/2 - nfc(1) + nfw/2, nmc(2) - faceinfo(4,4)/2 - nfc(2) + nfh/2, faceinfo(4,3:4)];
       
if nargin == 3
    face_image = imresize(face_image, [NaN, scale_to_size], 'bicubic');
    eye_info = eye_info * scale_to_size/nfw;
end
