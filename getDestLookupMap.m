function [destlm dest_a dest_b] = getDestLookupMap(destMarkings, fvidx)
destlm = zeros(floor(max(destMarkings(:,2)))+1, floor(max(destMarkings(:,1)))+1); % destination raster triangle lookup map 
dest_a = destlm;
dest_b = destlm;

N = size(fvidx,1);
for i = 1:N
    p0 = destMarkings(fvidx(i,:), :);
    [~, sortidx] = sort(p0(:,2));
    p = p0(sortidx, :);
    
    a_denu = [p0(3,2)-p0(1,2) -p0(3,1)+p0(1,1)]'/((p0(2,1)-p0(1,1))*(p0(3,2)-p0(1,2)) - (p0(2,2)-p0(1,2))*(p0(3,1)-p0(1,1)));
    b_denu = [-p0(2,2)+p0(1,2) p0(2,1)-p0(1,1)]'/((p0(2,1)-p0(1,1))*(p0(3,2)-p0(1,2)) - (p0(2,2)-p0(1,2))*(p0(3,1)-p0(1,1)));
   
    pmx = (p(3,1)-p(1,1))*(p(2,2)-p(1,2))/(p(3,2)-p(1,2)) + p(1,1);
    if pmx > p(2,1)
        dx1 = (p(2,1)-p(1,1))/(p(2,2)-p(1,2));
        dx2 = (p(3,1)-p(1,1))/(p(3,2)-p(1,2));
        dx3 = (p(3,1)-p(2,1))/(p(3,2)-p(2,2));
        dx4 = dx2;
        
        pm1 = p(2,:);
        pm2 = [pmx, p(2,2)];
    
    else
        dx1 = (p(3,1)-p(1,1))/(p(3,2)-p(1,2));
        dx2 = (p(2,1)-p(1,1))/(p(2,2)-p(1,2));
        dx3 = dx1;
        dx4 = (p(3,1)-p(2,1))/(p(3,2)-p(2,2));
    
        pm2 = p(2,:);
        pm1 = [pmx, p(2,2)];
    end
    
    
    if ~any(isinf([dx1, dx2]))
        x1 = p(1,1)+dx1*(round(p(1,2))+0.5-p(1,2));
        x2 = p(1,1)+dx2*(round(p(1,2))+0.5-p(1,2));
        for y = round(p(1,2)):round(pm1(2))-1;
            x = round(x1):round(x2)-1;
            if ~isempty(x)
                destlm(y, x) = i;

                dest_a(y, x) = ([x'+0.5-p0(1,1) repmat(y+0.5-p0(1,2), size(x,2),1)] * a_denu)';
                dest_b(y, x) = ([x'+0.5-p0(1,1) repmat(y+0.5-p0(1,2), size(x,2),1)] * b_denu)';
            end
            x1 = x1 + dx1;
            x2 = x2 + dx2;
        end
     end

    if ~any(isinf([dx3, dx4]))

        x1 = pm1(1)+dx3*(round(pm1(2))+0.5-pm1(2));
        x2 = pm2(1)+dx4*(round(pm1(2))+0.5-pm1(2));

        for y = round(pm1(2)):round(p(3,2))-1
            x = round(x1):round(x2)-1;
            if ~isempty(x)
                destlm(y, x) = i;

                dest_a(y, x) = ([x'+0.5-p0(1,1) repmat(y+0.5-p0(1,2), size(x,2),1)] * a_denu)';
                dest_b(y, x) = ([x'+0.5-p0(1,1) repmat(y+0.5-p0(1,2), size(x,2),1)] * b_denu)';
            end
            x1 = x1 + dx3;
            x2 = x2 + dx4;
        end
    end
    
end


end