
%% Generate Shape Basis
collectionpath = 'testcollect2';
markersavepath = 'marked';
markedfile = dir([collectionpath '/' markersavepath '/c*.mat']);
Nmarkers = length(markedfile);
S = zeros(100, Nmarkers);
for i = 1:Nmarkers
    load([collectionpath '/' markersavepath '/', markedfile(i).name]);
    S(:,i) = reshape(facemarking.markings, 100, 1);
end

[s0, s] = generateShapeBasis(S);
avgfacemarker = getStandardfacemarker(s0);

%% Generate Texture Basis
for i = 1:Nmarkers
    load([collectionpath '/' markersavepath '/', markedfile(i).name]);
    destImage = double(warpMarkings(facemarking.faceimg, facemarking.markings, avgfacemarker));
    %showmarkings(destImage/256, avgfacemarker);
    disp(markedfile(i).name);
    if i == 1
        [m, n, ~] = size(destImage);
        T = zeros(m*n*3, Nmarkers);
    end
    T(:,i) = double(reshape(destImage(:),m*n*3,1))/256;
end

[t0, t] = getTextureBasis(T);
avgfaceimg = reshape(t0, m, n, 3);

%%
RS = 10;
RT = 20;
 
[H, Hinv, SD] = getSDandHessian(s(:,1:RS), s0, t(:,1:RT), avgfaceimg);


%%

load testcollect2/c0029/succimgsave.mat
imgidx = 10;
srcimage = double(imread(person_success_images(imgidx).filename))/256;
q = getqfromfaceinfo(person_success_images(imgidx).faceinfo);

pq = [zeros(RS,1); q];


MAXITER = 50;
for i = 1:MAXITER
    pq = updateWarpAffinePQ(t0, s0, s(:,1:RS), srcimage, pq, SD, Hinv);
end

%%
load testcollect2/c0060/succimgsave.mat
imgidx = 2;
RS = 10;
srcimage = double(imread(person_success_images(imgidx).filename))/256;
q = getqfromfaceinfo(person_success_images(imgidx).faceinfo);
pqa = updateWarpAffinePQSimplex(t0, s0, s(:,1:RS), srcimage, [0.01*ones(RS,1); q; zeros(6,1)]);


target_s = getTranslate(s0, s(:,1:RS), pqa(1:RS), pqa(RS+1:RS+4));
target_markings = getStandardfacemarker(target_s);
figure;
showmarkings(srcimage, target_markings);
hold on
trimesh(getFaceVertexIndices, target_markings(:,1), target_markings(:,2));