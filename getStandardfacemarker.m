function facemarker = getStandardfacemarker(s)
    facemarker = reshape((s+0.5)*130, 50,2);
end