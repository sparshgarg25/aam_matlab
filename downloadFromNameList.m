function success_images = downloadFromNameList(namelist_str, start_idx, save_directory, image_count, image_max)

success_images = cell(length(namelist_str),1);

for person_id = start_idx:length(namelist_str)
    search_term = namelist_str{person_id};
    
    success_image_idx = 1;
    downloaded_image_idx = 1;
    person_success_images = struct;
    
    while (success_image_idx < image_count) && (downloaded_image_idx < image_max)
        fprintf('Downloading from #%d...\n', downloaded_image_idx);
        html_str = downloadGoogleSearchHTML(search_term, downloaded_image_idx);
  
        image_entries = extractGoogleImageURLs(html_str);
        save_path = [save_directory '/c' num2str( person_id, '%04d')];
        filename_prefix = ['c' num2str( person_id, '%04d')];
        
        [save_file_names download_success] = downloadImageEntries(image_entries, filename_prefix, save_path, downloaded_image_idx);
        
        downloaded_image_idx = downloaded_image_idx + length(image_entries);
        
        fprintf('Processing downloads...\n');
        
        for image_idx = 1:length(download_success) 
            if download_success(image_idx)
                try
                    detect_output = opencvtest(save_file_names{image_idx});
                    if (size(detect_output,1) == 4) && (detect_output(2,5) == 1) && (detect_output(3,5) == 1) && (detect_output(4,5) == -1)
                        fprintf('  Found single face...\n');
                        person_success_images(success_image_idx).filename = save_file_names{image_idx};
                        person_success_images(success_image_idx).faceinfo = detect_output;
                        person_success_images(success_image_idx).reference = image_entries(image_idx);
                        success_image_idx = success_image_idx+1;
                    end
                    
                catch err
                    fprintf('  OpenCV failure...%s\n', save_file_names{image_idx});
                end
            end
        end
    end
    save([save_path '/succimgsave'], 'person_success_images'); 
    success_images{person_id} = person_success_images;
end