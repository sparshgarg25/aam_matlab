function q = getqfromfaceinfo(faceinfo)
q = [0 0 0 0]';
e1c = faceinfo(2,1:2) + faceinfo(2,3:4)/2;
e2c = faceinfo(3,1:2) + faceinfo(3,3:4)/2;
mc = faceinfo(4,1:2) + faceinfo(4,3:4)/2;



etom_distance = mc(2)-(e1c(2)+e2c(2))/2;

q(1) = etom_distance/50-1;
fc = [(e1c(1) + e2c(1) + mc(1))/3, (e1c(2)+e2c(2))/2*31/50+mc(2)*19/50]'+faceinfo(1,1:2)';
q(3:4) = fc/130-0.5;
q(2) = -atan((e2c(2)-e1c(2))/(e2c(1)-e1c(1)));