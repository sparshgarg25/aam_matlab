function allmarkings = addBGMarking(image, markings)

[M N ~] = size(image);

bgmarkings = [1, 1;
    1 M/2;
    1 M+1;
    N/2 M+1;
    N+1 M+1;
    N+1 M/2;
    N+1, 1;
    N/2, 1];

allmarkings = [markings; bgmarkings];

end