function [faceimg, markings] = markface(filename, r, savetofile)

ef = 1.5; %expansion factor
fc = [r(1,1) + r(1,3)/2, r(1,2) + r(1,4)/2]; %face center
fr = floor([fc(1)-r(1,3)/2*ef, fc(1)+r(1,3)/2*ef, fc(2)-r(1,4)/2*ef, fc(2)+r(1,4)/2*ef]); % face region

img = imread(filename);

[m, n, ~] = size(img);

faceimg = img(max(1, fr(3)):min(n, fr(4)), max(1, fr(1)):min(m, fr(2)), :);

markings = zeros(50, 2);

hfig = gcf;
hold off
imshow(faceimg);

idx = 0;
while idx<50
    p = ginput(1);
    if p(1)<0 || p(2)<0
        if idx > 0
            idx = idx-1;
        else
            faceimg = [];
            return
        end
    else
        idx = idx + 1;
        markings(idx,:) = p;
        figure(hfig);
        showmarkings(faceimg, markings(1:idx, :));
    end
end

if nargin == 3
    facemarking.faceimg = faceimg;
    facemarking.markings = markings;
    save(savetofile, 'facemarking');
end

end
