%%
p_start = [0.1 0.0 0.2 0.45 -0.5]';


srcimage = double(reshape(T0, 130, 130, 3));



for i = 1:100
    
    p_update = updateWarpAffine(T0, s0, s(:,1:RS), srcimage, p_start, star_term, Hinv);
    s_start = s(:,1:RS)*p_start+s0;
    s_update = s(:,1:RS)*p_update+s0;
    figure(1)
    s_rshp = reshape(s_update + 1/128, 50,2)*128;
    showmarkings(srcimage/256,s_rshp);
    trimesh(fvidx, s_rshp(:,1), s_rshp(:,2));

    p_start = p_update
    movie_save(i) = getframe;

end

movie(movie_save)