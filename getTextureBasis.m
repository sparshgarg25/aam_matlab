function [t0, t] = getTextureBasis(T)
    
    t0 = mean(T,2);
    
    Nmarkers = size(T,2);
    
    [t sig tv] = svd(T-repmat(t0, 1, Nmarkers) ,0);
    
    figure
    plot(diag(sig),'-*');
    xlabel('Singular Value Index');
    ylabel('Singular Value');
    title('Texture Matrix Singular Values');
    
end