%%
fvidx = [11 12 21;
11 21 22;
11 22 23;
11 23 1;
1 23 24;
1 24 2;
1 2 25;
1 25 26;
1 26 16;
16 26 27;
16 27 28;
16 28 17;
12 13 21;
13 29 21;
21 29 30;
21 30 22;
22 30 23;
23 30 24;
24 30 32;
24 32 3;
2 24 3;
2 3 25;
3 33 25;
25 33 34;
25 34 26;
26 34 27;
27 34 28;
28 34 36;
28 36 18;
17 28 18;
13 31 29;
29 31 30;
30 31 32;
31 37 32;
3 32 37;
3 37 4;
3 4 38;
3 38 33;
33 38 35;
33 35 34;
34 35 36;
36 35 18;
13 14 39;
13 39 31;
31 39 37;
4 37 39;
4 40 38;
35 38 40;
18 35 40;
18 40 19;
14 41 39;
39 41 43;
39 43 5;
4 39 5;
4 5 40;
5 47 40;
40 47 42;
40 42 19;
5 43 6;
5 6 47;
14 15 41;
15 46 41;
15 9 46;
9 15 10;
9 10 20;
9 20 50;
20 42 50;
19 42 20;
41 44 43;
41 45 44;
41 46 45;
6 43 7;
7 43 44;
7 44 8;
8 44 45;
8 45 9;
9 45 46;
6 7 47;
7 48 47;
7 8 48;
8 49 48;
8 9 49;
9 50 49;
42 47 48;
42 48 49;
42 49 50];


bgidx = [51 11 58;
    51 12 11;
    51 52 12;
    12 52 13;
    52 14 13;
    52 53 14;
    53 15 14;
    53 54 15;
    15 54 10;
    10 54 20;
    20 54 55;
    20 55 19;
    19 55 56;
    18 19 56;
    17 18 56;
    16 17 57;
    58 16 57;
    58 1 16];

%%


load testcollect2/c0005/c0005_p0005.mat


n = length(fvidx);
figure(1);
[M N ~] = size(facemarking.faceimg);

bgmarkings = [0, 0;
    0 M/2;
    0 M;
    N/2 M;
    N M;
    N M/2;
    N, 0;
    N/2, 0];

all_markings = [facemarking.markings; bgmarkings];
all_indices = [fvidx; bgidx];

showmarkings(facemarking.faceimg, facemarking.markings);


%%
hold off
showmarkings(facemarking.faceimg, facemarking.markings);
hold on
trimesh(all_indices, all_markings(:,1), all_markings(:,2));
%%

patchinfo.Vertices = facemarking.markings;
patchinfo.Faces = fvidx;

p = patch(patchinfo);
cdata = hsv(length(fvidx));
     
set(p,'FaceColor','flat',...
'FaceVertexCData',cdata);


%%
[destlm desta destb] = getDestLookupMap(facemarking.markings, fvidx);

figure;
imagesc(destlm);
hold on
trimesh(fvidx, facemarking.markings(:,1), facemarking.markings(:,2));

figure;
imagesc(desta);
hold on
trimesh(fvidx, facemarking.markings(:,1), facemarking.markings(:,2));

figure;
imagesc(destb);
hold on
trimesh(fvidx, facemarking.markings(:,1), facemarking.markings(:,2));


%%

load testcollect2/c0004/c0004_p0003.mat

srcimage = facemarking.faceimg;
srcMarkings = facemarking.markings;

load testcollect2/c0005/c0005_p0001.mat
destimage = facemarking.faceimg;
destMarkings = facemarking.markings;

mappeddestimage = warpMarkings(srcimage, srcMarkings, destMarkings);

figure
subplot(1,3,1);
imshow(srcimage);
subplot(1,3,2);
imshow(destimage);
subplot(1,3,3);
imshow(mappeddestimage);

%%
destAllMarking = addBGMarking(destimage, destMarkings);
srcAllMarking = addBGMarking(srcimage, srcMarkings);

mappeddestimage = warpMarkings(destimage, destAllMarking, srcAllMarking);

figure
subplot(1,3,1);
imshow(srcimage);
subplot(1,3,2);
imshow(mappeddestimage);
subplot(1,3,3);
imshow(destimage);

%%

figure;
Nt = 5;
t = linspace(0,1,Nt);
for i = 1:Nt
    subplot(1,Nt, i);
    blendimage = blendbyMarkings(srcimage, addBGMarking(srcimage, srcMarkings), ...
        destimage, addBGMarking(destimage, destMarkings), t(i));
    imshow(blendimage/256);
end

