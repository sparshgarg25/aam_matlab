function html_str = downloadGoogleSearchHTML(search_term, start_idx)
% the follow google image search url is used
% q is the search term, start is the start image number
% http://www.google.com/search?hl=en&gbv=2&sout=1&biw=1361&bih=554&tbm=isch&q=%E6%B1%A4%E5%94%AF&start=40

    url_prefix = 'http://www.google.com/search?um=1&hl=en&biw=1540&bih=867&tbm=isch&ei=6yTlTvGTJseutwe8nLDvBA&q=';
    html_str = urlread([url_prefix search_term '&start=' num2str(start_idx) '&sa=N']);
    
end