%%
s = RandStream('mcg16807','Seed', 122611);

RandStream.setDefaultStream(s)

load facecollectsave

collectionpath = 'testcollect2';

np = 65; %number of person

meta_collect = {};

for i = 1:np
    load([collectionpath '/c' num2str(i, '%04d') '/succimgsave.mat']);
    for j = 1:length(person_success_images)
        meta_collect{end+1} = person_success_images(j);
    end
end

meta_collect = [meta_collect{:}];

N = 120;

S = randperm(size(meta_collect,2));

meta_selected = meta_collect(S(1:N));
marked = zeros(1,N);

for i = 120:N
    h = figure;
    fprintf('Marking %d: %s', i, meta_selected(i).filename);
    faceimg = markface(meta_selected(i).filename, meta_selected(i).faceinfo, meta_selected(i).filename(1:end-4));
    
    if isempty(faceimg)
        fprintf(' deselect\n');
    else
        fprintf(' done\n');
        marked(i) = 1;
    end
    close(h);
end


%%
collectionpath = 'testcollect2';

np = 65; %number of person
marked = 0;
for i = 1:np
    marked_savefile = dir([collectionpath '/c' num2str(i, '%04d') '/c*.mat']);
    marked = marked + size(marked_savefile,1);
end


%%
markersavepath = 'marked';
mkdir([collectionpath '/' markersavepath]);
for i = 1:np
    marked_savefile = dir([collectionpath '/c' num2str(i, '%04d') '/c*.mat']);
    for j = 1:length(marked_savefile)
        copyfile([collectionpath '/c' num2str(i, '%04d') '/', marked_savefile(j).name], ...
            [collectionpath '/' markersavepath '/', marked_savefile(j).name]);
    end
end

%%
markersavepath = 'marked';
markedfile = dir([collectionpath '/' markersavepath '/c*.mat']);
avgfacemarker = zeros(50,2);
Nmarkers = length(markedfile);
S = zeros(100, Nmarkers);
for i = 1:Nmarkers
    load([collectionpath '/' markersavepath '/', markedfile(i).name]);
    marker = facemarking.markings;
    markerspan = 2*max(abs([min(marker);max(marker)] - repmat(mean(marker), 2,1)));
    marker_normalized = (marker - repmat( mean(marker), 50, 1) )./repmat( markerspan, 50, 1) + repmat([0.5 0.5], 50, 1);
    avgfacemarker = avgfacemarker + marker_normalized;
    S(:,i) = marker_normalized(:);
end

avgfacemarker_normailzed2 = avgfacemarker/length(markedfile);
avgfacemarker_normailzed = (avgfacemarker_normailzed-repmat(min(avgfacemarker_normailzed), 50,1))./repmat(max(avgfacemarker_normailzed)-min(avgfacemarker_normailzed), 50,1);


%%

figure;
avgface_D = 128;
avgface_imgD = avgface_D+2;
avgfaceimage = uint8(255*ones(avgface_D, avgface_D, 3));
avgfacemarker = avgfacemarker_normailzed*avgface_D+repmat([1 1], 50, 1);
showmarkings(avgfaceimage, avgfacemarker);

%%
avgfaceimage = [];
figure
T = zeros(avgface_imgD*avgface_imgD*3, Nmarkers);
for i = 1:Nmarkers
    load([collectionpath '/' markersavepath '/', markedfile(i).name]);
    destImage = double(warpMarkings(facemarking.faceimg, facemarking.markings, avgfacemarker));
    hold off
    %showmarkings(destImage/256, avgfacemarker);
    disp(markedfile(i).name);
    if isempty(avgfaceimage)
        avgfaceimage = double(zeros(size(destImage)));
    end
    T(:,i) = destImage(:);
    avgfaceimage = avgfaceimage + destImage;
end


%% average face
figure
imshow(avgfaceimage/Nmarkers/256)




%%

s0 = avgfacemarker_normailzed(:);

[s sig sv] = svd(S-repmat(s0(:), 1, Nmarkers) ,0);

%%
figure
plot(diag(sig),'-*');
xlabel('Singular Value Index');
ylabel('Singular Value');
title('Shape Matrix Singular Values');

%%
for i = 1:8
    figure
    showmarkings(ones(130,130), reshape(s0*130, 50,2));
    quiver(s0(1:50)*130, s0(51:100)*130, s(1:50,i)*130, s(51:100,i)*130, 'r')
    set(gcf, 'Position', [50 50 500 500])
end




%% texture SVD


T0 = avgfaceimage(:)/Nmarkers;

[t sig_t tv] = svd(T-repmat(T0(:), 1, Nmarkers) ,0);

%%


figure
plot(diag(sig_t),'-*');
xlabel('Singular Value Index');
ylabel('Singular Value');
title('Texture Matrix Singular Values');



%%
close all
figure
imshow((reshape(T0, 130, 130, 3))/256);
set(gcf, 'Position', [50 50 500 500])
for i = 1:4
    ti = t(:,i);
    figure
    imshow((reshape(t(:,i)*10000+T0, 130, 130, 3))/256);
    set(gcf, 'Position', [50 50 500 500])
end


%%
figure
set(gcf, 'Position', [50 50 500 500])
for i = 1:50
    ti = t(:,i);
    imshow((reshape(t(:,i)*1000+T0, 130, 130, 3))/256);
    pause
end


%%
RS = 5;
[fvidx bgidx] = getFaceVertexIndices();

[destlm dest_a dest_b] = getDestLookupMap(avgfacemarker, fvidx);
[m,n] = size(destlm);

dw_dx = zeros(m, n, 50);

for i = 1:50
    [Tidx ~] = find(fvidx == i);
    for j = 1:length(Tidx)
        triangle_indices =  fvidx(Tidx(j),:);
        other_vectices = triangle_indices(triangle_indices ~= i);
        p0 = zeros(3,2);
        p0(1,:) = avgfacemarker(i,:);
        p0(2,:) = avgfacemarker(other_vectices(1),:);
        p0(3,:) = avgfacemarker(other_vectices(2),:);
       
        a_denu = [p0(3,2)-p0(1,2) -p0(3,1)+p0(1,1)]'/((p0(2,1)-p0(1,1))*(p0(3,2)-p0(1,2)) - (p0(2,2)-p0(1,2))*(p0(3,1)-p0(1,1)));
        b_denu = [-p0(2,2)+p0(1,2) p0(2,1)-p0(1,1)]'/((p0(2,1)-p0(1,1))*(p0(3,2)-p0(1,2)) - (p0(2,2)-p0(1,2))*(p0(3,1)-p0(1,1)));
   
        [pixel_y pixel_x] = find(destlm == Tidx(j));
        for k = 1:length(pixel_x)
            dw_dx(pixel_y(k), pixel_x(k), i) = 1 - ([pixel_x(k)+0.5 pixel_y(k)+0.5] - p0(1,:))*(a_denu + b_denu);
        end
    end
end


%%

figure
for i = 1:50
    imshow(dw_dx(:,:,i));
    pause;
end


%%
dw_dp_x = zeros(m, n, RS);
dw_dp_y = zeros(m, n, RS);
for i = 1:50
    for j = 1:RS
        dw_dp_x(:,:,j) = dw_dp_x(:,:,j) + s(i,j)*dw_dx(:,:,i);
        dw_dp_y(:,:,j) = dw_dp_y(:,:,j) + s(i+50,j)*dw_dx(:,:,i);  
    end
end


%%
close all
figure
for i = 1:RS
    subplot(2,1,1);
    imshow(dw_dp_x(:,:,i), [-0.5,0.5]);
    subplot(2,1,2);
    imshow(dw_dp_y(:,:,i), [-0.5,0.5]);
    pause;
end

%%

T0img = reshape(T0, 130, 130, 3);
T0img_x = zeros(size(T0img));
T0img_x(:, 1:129, :) = T0img(:, 2:130, :);
T0img_y = zeros(size(T0img));
T0img_y(1:129, :, :) = T0img(2:130, :, :);
T0imgdx = T0img_x - T0img;
T0imgdy = T0img_y - T0img;


figure;

subplot(2,1,1);
imshow(T0imgdx(:,:,1)/256, [-0.1, 0.1]);
subplot(2,1,2);
imshow(T0imgdy(:,:,1)/256, [-0.1, 0.1]);


%% Calculate Hessian
star_term = zeros(m, n, 3, RS);


for i = 1:RS
    for j = 1:3
        star_term(:,:,j,i) = T0imgdx(:,:,j) .* dw_dp_x(:,:,i) + T0imgdy(:,:,j) .* dw_dp_y(:,:,i);
    end
end

H = zeros(RS, RS);
star_term_at_pixel = zeros(3, RS);
for x = 1:n
    for y = 1:m
        star_term_at_pixel(:,:) = star_term(y,x,:,:);
        H = H + star_term_at_pixel' * star_term_at_pixel;
    end
end

Hinv = inv(H);
%%

load testcollect2/marked/c0060_p0018.mat
test_img = facemarking.faceimg(44:214, 40:195,:);



%%
close all
test_img = reshape(T0, 130, 130, 3);
p_new = [0.0 0.0 0.2 0.5 0.0]';

%%
p_target = p_new;
close all

p0 = zeros(RS,1);

s_target = s(:,1:RS)*p_target + s0;
s_0 = s(:,1:RS)*p0 + s0;
subplot(1,4,1);
showmarkings(test_img/256,reshape(s_target*128+1, 50, 2));

subplot(1,4,2);
destImage = warpMarkings(test_img, reshape(s_target*128+1, 50, 2), reshape(s_0*128+1, 50, 2));

showmarkings(destImage/256, reshape(s_0*128+1, 50, 2));

Idiff = double(destImage) - double(reshape(T0, 130, 130, 3));
subplot(1,4,3);
imshow(Idiff/256);
subplot(1,4,4);
imshow(-Idiff/256);


star_transpose_Idiff = zeros(RS,1);
Idiff_at_pixel = zeros(3,1);
for x = 1:n
    for y = 1:m
        star_term_at_pixel(:,:) = star_term(y,x,:,:);
        Idiff_at_pixel(:,1) = Idiff(y,x,:);
        star_transpose_Idiff(:,:) = star_transpose_Idiff(:,:) + star_term_at_pixel'*Idiff_at_pixel;
    end
end
dp = Hinv*star_transpose_Idiff/256;
 
ds_0 = -s(:,1:RS)*dp;

figure

subplot(1,3,1);
%showmarkings(test_img/256,reshape((s0+ds_0)*128+1, 50, 2));
quiver(s0(1:50), s0(51:100), ds_0(1:50,1), ds_0(51:100,1))
hold on
trimesh(fvidx, s0(1:50), s0(51:100));
set(gca,'YDir','reverse')

ds = getIncrementalWarp(s0, ds_0, s_target);
p_new = s(:,1:RS)' * (s_target+ds - s0)


subplot(1,3,2);
quiver(s_target(1:50), s_target(51:100), ds(1:50,1), ds(51:100,1))
hold on
trimesh(fvidx, s_target(1:50), s_target(51:100));
set(gca,'YDir','reverse')

subplot(1,3,3);
trimesh(fvidx, s_target(1:50)+ds(1:50,1), s_target(51:100)+ds(51:100,1));
set(gca,'YDir','reverse')


%showmarkings(test_img/256,reshape(s_target+ds+1/128, 50,2)*128);
