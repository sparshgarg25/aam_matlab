function [face_collect meta_collect] = collectAllColorFaces(foldername, idx)
 
    face_collect = zeros(141*128*3,0);
    meta_collect = {};
    face_count = 1;
    
    for i=idx
        cname = [foldername '/c' num2str(i, '%04d')];
        load([cname '/succimgsave.mat']);
        N = length(person_success_images);
        
        for j = 1:N
            img = imread(person_success_images(j).filename);
            r = person_success_images(j).faceinfo;
            [face_image, eye_info] = extractFaceImage(img, r, 128);
            
            if eye_info(1,3)/eye_info(2,3) < 1.1 && eye_info(1,3)/eye_info(2,3)>0.909
                
                [m,n,~] = size(face_image);
                
                if all([m,n] == [141 128])
                    
                    face_collect(:,face_count) = reshape(face_image(1:141,1:128,:), 141*128*3, 1);
                    person_success_images(j).eyeinfo = eye_info;
                    person_success_images(j).personid = i;
                    
                    meta_collect{face_count} = person_success_images(j);
                    face_count = face_count+1;
                end
            
            end

       
        end    
    end
    
    meta_collect = [meta_collect{:}];
end